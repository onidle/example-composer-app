# Example app
Cakephp example app

## [Version 1.0.6](#markdown-header-version-106)
* Releasing 1.0.6 version
* README file versions headliens amended
* Trying to fix anchor for bitbucket #3
* Trying to fix anchor for bitbucket #2
* Trying to fix anchor for bitbucket
* Linking headlines so it is easy to link to changelog

## [Version 1.0.5](#markdown-header-version-105)
* Removing sha hash from README file list
* Making change log as list
* Putting auto link to commit is not possible, reverting back
* Trying to fix README file
* README file added

## [Version 1.0.4](#markdown-header-version-104)
* Remove pwd command from post install script in composer.json
* Typo fix in composer.json file
* Trying post install cmd scripts
* Add needed folders (empty files)

## [Version 1.0.3](#markdown-header-version-103)
* Removing Plugin & Vendor dirs from app folder
* Loading debug kit toolbar
* Changing security values to app specific
* Making cake lib and plugins load correctly regards to composer setting
* Adding cake skeleton and amending .gitignore

## [Version 1.0.2](#markdown-header-version-102)
* Updating composer.json for showing how dependencies works

## [Version 1.0.1](#markdown-header-version-101)
* Adding on-IDLE specific plugin into composer.json

## [Version 1.0.0](#markdown-header-version-100)
* Updating composer.json and .gitignore

## [Version 0.1.0](#markdown-header-version-010)
* Initial commit

